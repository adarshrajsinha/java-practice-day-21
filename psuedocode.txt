CGI Pseudocode questions

Answer 1)

Prompt the user to enter the radius of the circle
Read the value of the radius from the user

If the radius is less than or equal to 0
    Print an error message "Invalid radius. Please enter a positive value."
Else
    Calculate the area of the circle using the formula:
        area = 3.14 * radius * radius

    Print the calculated area of the circle
End if


Answer 2) 

Prompt the user to enter a number
Read the value of the number from the user

For each multiplier from 1 to 10
    Calculate the product by multiplying the number with the multiplier
    Print the multiplication expression: number * multiplier = product
End for


Answer 3)

Prompt the user to enter a number
Read the value of the number from the user

Set the sum variable to 0

For each divisor from 1 to (number - 1)
    If the number is divisible by the divisor
        Add the divisor to the sum variable
    End if
End for

If the sum is equal to the number
    Print "The number is perfect"
Else
    Print "The number is not perfect"
End if


Answer 4)

Prompt the user to enter the first number
Read the value of the first number from the user

Prompt the user to enter the second number
Read the value of the second number from the user

Prompt the user to enter the third number
Read the value of the third number from the user

Set the lowest variable to the first number

If the second number is less than the lowest variable
    Set the lowest variable to the second number
End if

If the third number is less than the lowest variable
    Set the lowest variable to the third number
End if

Print the value of the lowest variable as the lowest number among the three


Answer 5)

Prompt the user to enter the first number
Read the value of the first number from the user

Prompt the user to enter the second number
Read the value of the second number from the user

Print "Before swapping:"
Print "First number: " + firstNumber
Print "Second number: " + secondNumber

Set firstNumber to (firstNumber + secondNumber)
Set secondNumber to (firstNumber - secondNumber)
Set firstNumber to (firstNumber - secondNumber)

Print "After swapping:"
Print "First number: " + firstNumber
Print "Second number: " + secondNumber


Answer 6)

Prompt the user to enter a number
Read the value of the number from the user

Set isPerfectSquare variable to false

If the number is greater than or equal to 0
    Calculate the square root of the number and round it to the nearest integer
    Multiply the rounded square root by itself to get the result

    If the result is equal to the original number
        Set isPerfectSquare variable to true
    End if
End if

If isPerfectSquare is true
    Print "The number is a perfect square"
Else
    Print "The number is not a perfect square"
End if


Answer 7)

Prompt the user to enter a number from 1 to 7
Read the value of the number from the user

If the number is 1
    Print "Sunday"
Else if the number is 2
    Print "Monday"
Else if the number is 3
    Print "Tuesday"
Else if the number is 4
    Print "Wednesday"
Else if the number is 5
    Print "Thursday"
Else if the number is 6
    Print "Friday"
Else if the number is 7
    Print "Saturday"
Else
    Print "Invalid input. Please enter a number from 1 to 7."
End if


Answer 8)

Prompt the user to enter the first number
Read the value of the first number from the user

Prompt the user to enter the second number
Read the value of the second number from the user

Display the calculator menu options:
Print "Select an operation:"
Print "1. Addition (+)"
Print "2. Subtraction (-)"
Print "3. Multiplication (*)"
Print "4. Division (/)"

Prompt the user to select an operation
Read the choice of operation from the user

If the choice is 1
    Perform addition: result = firstNumber + secondNumber
    Print "The result of addition is: " + result
Else if the choice is 2
    Perform subtraction: result = firstNumber - secondNumber
    Print "The result of subtraction is: " + result
Else if the choice is 3
    Perform multiplication: result = firstNumber * secondNumber
    Print "The result of multiplication is: " + result
Else if the choice is 4
    If the second number is not equal to 0
        Perform division: result = firstNumber / secondNumber
        Print "The result of division is: " + result
    Else
        Print "Error: Division by zero is not allowed"
    End if
Else
    Print "Invalid choice. Please select a valid operation."
End if


Answer 9) 

Prompt the user to enter the StudentID
Read the value of the StudentID from the user

Prompt the user to enter the StudentName
Read the value of the StudentName from the user

Prompt the user to enter the StudentAge
Read the value of the StudentAge from the user

Prompt the user to enter the Marks1
Read the value of Marks1 from the user

Prompt the user to enter the Marks2
Read the value of Marks2 from the user

Prompt the user to enter the Marks3
Read the value of Marks3 from the user

Set Total to the sum of Marks1, Marks2, and Marks3

Set Percentage to (Total / 3)

If Percentage is greater than or equal to 50
    Print "PASS"
Else
    Print "FAIL"
End if


Answer 10)

Set firstNumber to 0
Set secondNumber to 1

Print firstNumber
Print secondNumber

Set nextNumber to firstNumber + secondNumber

While nextNumber is less than or equal to 100
    Print nextNumber
    Set firstNumber to secondNumber
    Set secondNumber to nextNumber
    Set nextNumber to firstNumber + secondNumber
End while
